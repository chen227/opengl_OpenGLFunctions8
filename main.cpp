﻿#include <QtGui/QGuiApplication>
#include "openglwindow.h"
#include "trianglewindow.h"

int main(int argc, char **argv)
{
    QGuiApplication app(argc, argv);

    QSurfaceFormat format;
    //设置采样率
    format.setSamples(16);

    TriangleWindow window;
    window.setTitle(QStringLiteral("第八课：融合"));
    window.setFormat(format);
    window.resize(640, 480);
    window.setAnimating(true);
    window.show();

    return app.exec();
}
